import CompositionRoot from '../../compositionRoot';
import {
    mockConfig,
    mockUser1,
    mockUser2,
    mockUser3,
} from './integration.test.data';

export const initCompositionRoot = async (): Promise<CompositionRoot> => {
    const compositionRoot = new CompositionRoot();
    await compositionRoot.createServer(mockConfig);
    return compositionRoot;
};

export const initRestService = (compositionRoot: CompositionRoot) => {
    const restService = compositionRoot.getRestService();
    restService.init();
    return restService;
};

export const prepareMockUsers = async (compositionRoot: CompositionRoot) => {
    const userService = compositionRoot.getUserService();
    const passwordManager = compositionRoot.getPasswordManager();
    await Promise.all([
        mockUser1,
        mockUser2,
        mockUser3,
    ].map((mockUser) => {
        mockUser.password = passwordManager.encodePassword(mockUser.password);
        return userService.createUser(mockUser);
    }));
};
