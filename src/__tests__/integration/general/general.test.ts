import { Express } from 'express';
import request from 'supertest';

import TokenManager from '../../../tools/tokenManager';
import { initCompositionRoot, initRestService } from '../integration.test.utils';
import {
    notFoundGetPath,
    notFoundResponseBody,
    serverErrorResponseBody,
} from './general.test.data';

describe('General integration tests', () => {
    let app: Express;
    let staticToken: string;

    beforeAll(async () => {
        const compositionRoot = await initCompositionRoot();
        const restService = initRestService(compositionRoot);
        app = restService.getApp();

        const tokenManager = compositionRoot.getTokenManager();
        staticToken = tokenManager.getStaticToken();
    });

    it('Should handle internal server errors', () => {
        jest.spyOn(TokenManager.prototype, 'validateJwtToken').mockImplementation(() => {
            throw new Error('test');
        });
        return request(app)
            .get('/users/1')
            .set('authorization', 'Bearer 1')
            .expect(500)
            .expect(serverErrorResponseBody);
    });
    it('Should handle not found errors', () => {
        return request(app)
            .get(notFoundGetPath)
            .expect(404)
            .expect(notFoundResponseBody);
    });
});
