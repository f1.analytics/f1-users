import { Express } from 'express';
import jwt from 'jsonwebtoken';
import request from 'supertest';

import { IUserService } from '../../../services/userService';
import { mockUser1, mockUser2 } from '../integration.test.data';
import {
    initCompositionRoot,
    initRestService,
    prepareMockUsers,
} from '../integration.test.utils';
import {
    expectedBadRequestPostUserResponse,
    expectedEmailTakenPostUserResponse,
    expectedNotFoundGetUserResponse,
    expectedUnauthorizedGetUserResponse,
    expectedUsernameTakenPostUserResponse,
    mockExistingEmailNewUser,
    mockExistingUsernameNewUser,
    mockInvalidNewUser,
    mockNewUser,
} from './users.test.data';

describe('Users integration tests', () => {
    let app: Express;
    let userService: IUserService;

    const getMockUser = (id: string, isSuperuser: boolean) => ({ payload: { id: Number(id), isSuperuser } });

    beforeAll(async () => {
        const compositionRoot = await initCompositionRoot();
        const restService = initRestService(compositionRoot);

        await prepareMockUsers(compositionRoot);

        app = restService.getApp();
        userService = compositionRoot.getUserService();
    });

    describe('GET /:id tests', () => {
        it('Should get user if request from self', async () => {
            jest.spyOn(jwt, 'verify').mockImplementation((id) => getMockUser(id, false));

            return request(app)
                .get('/users/1')
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect(mockUser1);
        });
        it('Should get user if request from superuser', async () => {
            jest.spyOn(jwt, 'verify').mockImplementation((id) => getMockUser(id, true));

            return request(app)
                .get('/users/2')
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect(mockUser2);
        });
        it('Should not get user if request from non self or superuser', async () => {
            jest.spyOn(jwt, 'verify').mockImplementation((id) => getMockUser(id, false));

            return request(app)
                .get('/users/2')
                .set('Authorization', 'Bearer 1')
                .expect(401)
                .expect(expectedUnauthorizedGetUserResponse);
        });
        it('Should not get user if user does not exit', async () => {
            jest.spyOn(jwt, 'verify').mockImplementation((id) => getMockUser(id, true));

            return request(app)
                .get('/users/2222')
                .set('Authorization', 'Bearer 1')
                .expect(404)
                .expect(expectedNotFoundGetUserResponse);
        });
    });
    describe('POST / tests', () => {
        it('Should register new user', () => {
            return request(app)
                .post('/users')
                .send(mockNewUser)
                .expect(200)
                .then(async () => {
                    const user = await userService.getUserWithLogin({
                        email: mockNewUser.email,
                        password: mockNewUser.password,
                    });
                    expect(user.email).toBe(mockNewUser.email);
                });
        });
        it('Should not register new user if bad request', () => {
            return request(app)
                .post('/users')
                .send(mockInvalidNewUser)
                .expect(400)
                .expect(expectedBadRequestPostUserResponse);
        });
        it('Should not register new user if email exists', () => {
            return request(app)
                .post('/users')
                .send(mockExistingEmailNewUser)
                .expect(409)
                .expect(expectedEmailTakenPostUserResponse);
        });
        it('Should not register new user if username exists', () => {
            return request(app)
                .post('/users')
                .send(mockExistingUsernameNewUser)
                .expect(409)
                .expect(expectedUsernameTakenPostUserResponse);
        });
    });
});
