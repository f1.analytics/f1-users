import { errorCodes } from '../../../errors';
import { mockUser1 } from '../integration.test.data';

export const mockNewUser = {
    email: 'new@gmail.com',
    password: '2312dsa21e',
    username: 'newUser',
};
export const mockInvalidNewUser = {
    email: 'test@gmail.com',
    password: 'hello',
};
export const mockExistingEmailNewUser = {
    email: mockUser1.email,
    password: '2312dsa21e',
    username: 'newUser213123',
};
export const mockExistingUsernameNewUser = {
    email: 'email@gmail.com2334',
    password: '2312dsa21e',
    username: mockUser1.username,
};

export const expectedUnauthorizedGetUserResponse = {
    error: {
        code: errorCodes.UNAUTHORIZED,
        message: 'User 1 is not allowed to access user 2',
    },
};
export const expectedNotFoundGetUserResponse = {
    error: {
        code: errorCodes.USER_NOT_FOUND,
        message: 'User 2222 does not exist',
    },
};
export const expectedBadRequestPostUserResponse = {
    error: {
        code: errorCodes.BAD_REQUEST,
        message: 'should have required property \'username\'',
    },
};
export const expectedEmailTakenPostUserResponse = {
    error: {
        code: errorCodes.EMAIL_TAKEN,
        message: 'Email test1@gmail.com is already taken',
    },
};
export const expectedUsernameTakenPostUserResponse = {
    error: {
        code: errorCodes.USERNAME_TAKEN,
        message: 'Username test1 is already taken',
    },
};
