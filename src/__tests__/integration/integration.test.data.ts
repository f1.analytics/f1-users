import { IJwtConfig } from '../../types';

export const mockConfig = {
    api: {
        dashboards: 'http://localhost:8081',
    },
    jwt: {
        accessTokenLifeTime: '1d',
        refreshTokenLifeTime: '7d',
    },
    logger: {
        level: 'info',
    },
    persistence: {
        database: ':memory:',
        dropSchema: true,
        logging: false,
        synchronize: true,
        type: 'sqlite',
    },
    server: {
        host: 'test',
        port: 1234,
    },
};

export const mockUser1 = {
    email: 'test1@gmail.com',
    id: 1,
    isSuperuser: true,
    password: 'test1234',
    username: 'test1',
};
export const mockUser2 = {
    email: 'test2@gmail.com',
    id: 2,
    isSuperuser: false,
    password: '123rewrfsda',
    username: 'test2',
};
export const mockUser3 = {
    email: 'test3@gmail.com',
    id: 3,
    isSuperuser: false,
    password: '1231sadasd',
    username: 'test3',
};
