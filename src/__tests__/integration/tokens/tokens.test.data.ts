import { errorCodes } from '../../../errors';
import { mockUser1 } from '../integration.test.data';

export const mockValidLogin = {
    email: mockUser1.email,
    password: mockUser1.password,
};
export const mockInvalidEmailLogin = {
    email: 'invalidEmail@gmail.com',
    password: mockUser1.password,
};
export const mockInvalidPasswordLogin = {
    email: mockUser1.email,
    password: 'wrong',
};

export const expectedInvalidLoginResponse = {
    error: {
        code: errorCodes.WRONG_CREDENTIALS,
        message: 'Wrong credentials',
    },
};
export const expectedInvalidRefreshTokenResponse = {
    error: {
        code: errorCodes.UNAUTHORIZED,
        message: 'Invalid json web token: jwt malformed',
    },
};
