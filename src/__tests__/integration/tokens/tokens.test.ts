import { Express } from 'express';
import request from 'supertest';

import { ITokenManager } from '../../../tools/tokenManager';
import { mockUser1 } from '../integration.test.data';
import {
    initCompositionRoot,
    initRestService,
    prepareMockUsers,
} from '../integration.test.utils';
import {
    expectedInvalidLoginResponse, expectedInvalidRefreshTokenResponse,
    mockInvalidEmailLogin, mockInvalidPasswordLogin,
    mockValidLogin,
} from './tokens.test.data';

describe('Tokens integration tests', () => {
    let app: Express;
    let tokenManager: ITokenManager;

    beforeAll(async () => {
        const compositionRoot = await initCompositionRoot();
        const restService = initRestService(compositionRoot);

        await prepareMockUsers(compositionRoot);

        app = restService.getApp();
        tokenManager = compositionRoot.getTokenManager();
    });

    describe('POST / tests', () => {
        it('Should create access and refresh tokens',  () => {
            return request(app)
                .post('/tokens')
                .send(mockValidLogin)
                .expect(200)
                .then((res) => {
                    expect(typeof res.body.accessToken).toBe('string');
                    expect(typeof res.body.refreshToken).toBe('string');
                    expect(res.body.userId).toBe(mockUser1.id);
                });
        });
        it('Should not create tokens if email is invalid', () => {
            return request(app)
                .post('/tokens')
                .send(mockInvalidEmailLogin)
                .expect(401)
                .expect(expectedInvalidLoginResponse);
        });
        it('Should not create tokens if password is invalid', () => {
            return request(app)
                .post('/tokens')
                .send(mockInvalidPasswordLogin)
                .expect(401)
                .expect(expectedInvalidLoginResponse);
        });
    });
    describe('POST /refresh tests', () => {
        it('Should refresh access token', async () => {
            const refreshToken = tokenManager.createJwtRefreshToken(mockUser1);
            return request(app)
                .post('/tokens/refresh')
                .send({ refreshToken })
                .expect(200)
                .then((res) => {
                    expect(typeof res.body.accessToken).toBe('string');
                });
        });
        it('Should not refresh access token with invalid refreshToken', async () => {
            return request(app)
                .post('/tokens/refresh')
                .send({ refreshToken: 'random' })
                .expect(401)
                .expect(expectedInvalidRefreshTokenResponse);
        });
    });
});
