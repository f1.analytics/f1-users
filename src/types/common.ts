export interface IConfig {
    server: IServerConfig;
    logger: ILoggerConfig;
    persistence: IPersistenceConfig;
    jwt: IJwtConfig;
    api: IApiConfig;
}

export interface IServerConfig {
    port: number;
    host: string;
}

export interface ILoggerConfig {
    level: string;
}

export interface IPersistenceConfig {
    type: string;
    host?: string;
    database: string;
    port?: number;
    dropSchema?: boolean;
    logging?: boolean;
    synchronize?: boolean;
}

export interface IJwtConfig {
    accessTokenLifeTime: string;
    refreshTokenLifeTime: string;
}

export interface IApiConfig {
    dashboards: string;
}

export interface IUser {
    username: string;
    email: string;
    id: number;
    isSuperuser: boolean;
}

export interface ICredentials {
    email: string;
    password: string;
}
