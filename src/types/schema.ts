import { ICredentials } from './common';

export interface ICreateTokensInput extends ICredentials {}

export interface IRefreshTokenInput {
    refreshToken: string;
}

export interface ICreateUserInput {
    email: string;
    password: string;
    username: string;
}
