export interface IDashboardsClient {
    createDefaultUserDashboards(userId: number): Promise<void>;
}
