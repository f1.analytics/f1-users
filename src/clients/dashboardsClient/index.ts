import DashboardsClient from './dashboardsClient';

export * from './dashboardsClient.types';
export default DashboardsClient;
