import axios from 'axios';

import { ITokenManager } from '../../tools/tokenManager';
import { IDashboardsClient } from './dashboardsClient.types';

class DashboardsClient implements IDashboardsClient {
    private readonly host: string;
    private readonly tokenManager: ITokenManager;

    public constructor(host: string, tokenManager: ITokenManager) {
        this.host = host;
        this.tokenManager = tokenManager;
    }

    public async createDefaultUserDashboards(userId: number): Promise<void> {
        try {
            await axios.post(`${this.host}/dashboards/default`, {
                userId,
            }, {
                headers: {
                    authorization: this.tokenManager.getStaticToken(),
                },
            });
        } catch (error) {
            if (error.response) {
                throw new Error(error.response.data.message);
            } else {
                throw error;
            }
        }
    }
}

export default DashboardsClient;
