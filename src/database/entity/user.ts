import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
class User {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ length: '80' })
    @Index({ unique: true })
    public username: string;

    @Column({ length: '300' })
    @Index({ unique: true })
    public email: string;

    @Column({ length: '80' })
    @Column('text')
    public password: string;

    @Column({
        default: false,
        type: 'boolean',
    })
    public isSuperuser: boolean;
}

export default User;
