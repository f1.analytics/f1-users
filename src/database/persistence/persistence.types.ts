export interface IPersistence {
    init(): Promise<void>;
}

export enum PersistenceErrorTypes {
    EntityNotFound = 'EntityNotFound',
    QueryFailedError = 'QueryFailedError',
}
