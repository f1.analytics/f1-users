import { EntityRepository, Repository } from 'typeorm';

import { ICreateUserInput } from '../../types';
import User from '../entity/user';

@EntityRepository(User)
class UserRepository extends Repository<User> {
    public getUser(id: number): Promise<User> {
        return this.findOneOrFail({
            where: {
                id,
            },
        });
    }

    public getUserEmail(email: string): Promise<User> {
        return this.findOneOrFail({
            where: {
                email,
            },
        });
    }

    public getUsers(): Promise<User[]> {
        return this.find();
    }

    public async saveUser(userInput: ICreateUserInput): Promise<User> {
        const user = await this.create(userInput);
        return await this.save(user);
    }
}

export default UserRepository;
