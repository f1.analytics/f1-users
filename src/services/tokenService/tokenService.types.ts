import { ICreateTokensInput } from '../../types';

export interface ITokenService {
    createTokens(createTokensInput: ICreateTokensInput): Promise<ICreatedTokensType>;
    refreshToken(refreshToken: string): Promise<string>;
}

export interface ICreatedTokensType {
    accessToken: string;
    refreshToken: string;
    userId: number;
}
