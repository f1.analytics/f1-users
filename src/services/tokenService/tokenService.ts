import { ITokenManager } from '../../tools/tokenManager';
import { ICreateTokensInput } from '../../types';
import { IUserService } from '../userService';
import { ICreatedTokensType, ITokenService } from './tokenService.types';

class TokenService implements ITokenService {
    private readonly tokenManager: ITokenManager;
    private readonly userService: IUserService;

    public constructor(tokenManager: ITokenManager, userService: IUserService) {
        this.tokenManager = tokenManager;
        this.userService = userService;
    }

    public async createTokens(createTokensInput: ICreateTokensInput): Promise<ICreatedTokensType> {
        const user = await this.userService.getUserWithLogin(createTokensInput);
        const tokenPayload = {
            email: user.email,
            id: user.id,
            isSuperuser: user.isSuperuser,
            username: user.username,

        };
        const accessToken = this.tokenManager.createJwtAccessToken(tokenPayload);
        const refreshToken = this.tokenManager.createJwtRefreshToken(tokenPayload);

        return { accessToken, refreshToken, userId: user.id };
    }

    public async refreshToken(refreshToken: string): Promise<string> {
        const { payload } = this.tokenManager.validateJwtToken(refreshToken);
        return this.tokenManager.createJwtAccessToken(payload);
    }
}

export default TokenService;
