import { getCustomRepository } from 'typeorm';

import { IDashboardsClient } from '../../clients/dashboardsClient';
import User from '../../database/entity/user';
import { PersistenceErrorTypes } from '../../database/persistence';
import UserRepository from '../../database/repository/userRepository';
import { EmailTakenError, UsernameTakenError, UserNotFound, WrongCredentialsError } from '../../errors/requestErrors';
import { IPasswordManager } from '../../tools/passwordManager';
import { ICreateUserInput, ICredentials } from '../../types';
import { IUserService } from './userService.types';

class UserService implements IUserService {
    private readonly repository: UserRepository;
    private readonly passwordManager: IPasswordManager;
    private readonly dashboardsClient: IDashboardsClient;

    public constructor(passwordManager: IPasswordManager, dashboardsClient: IDashboardsClient) {
        this.repository = getCustomRepository(UserRepository);
        this.passwordManager = passwordManager;
        this.dashboardsClient = dashboardsClient;
    }

    public async getUser(id: number): Promise<User> {
        try {
            return await this.repository.getUser(id);
        } catch (error) {
            if (error.name === PersistenceErrorTypes.EntityNotFound) {
                throw new UserNotFound(`User ${id} does not exist`);
            } else {
                throw error;
            }
        }
    }

    public async getUserWithLogin({ email, password }: ICredentials): Promise<User> {
        try {
            const user = await this.repository.getUserEmail(email);
            await this.passwordManager.validatePassword(password, user.password);
            return user;
        } catch (error) {
            if (error.name === PersistenceErrorTypes.EntityNotFound) {
                throw new WrongCredentialsError('Wrong credentials');
            } else {
                throw error;
            }
        }
    }

    public getUsers(): Promise<User[]> {
        return this.repository.getUsers();
    }

    public async registerUser(userInput: ICreateUserInput): Promise<void> {
        userInput.password = this.passwordManager.encodePassword(userInput.password);
        await this.createUser(userInput);
    }

    public async createUser(userInput: ICreateUserInput): Promise<void> {
        try {
            await this.repository.saveUser(userInput);
        } catch (error) {
            if (this.isUniqueEmailError(error)) {
                throw new EmailTakenError(`Email ${userInput.email} is already taken`);
            } else if (this.isUniqueUsernameError(error)) {
                throw new UsernameTakenError(`Username ${userInput.username} is already taken`);
            }
            throw error;
        }
    }

    private isUniqueEmailError(error: Error): boolean {
        return error.name === PersistenceErrorTypes.QueryFailedError && error.message.includes('user.email');
    }

    private isUniqueUsernameError(error: Error): boolean {
        return error.name === PersistenceErrorTypes.QueryFailedError && error.message.includes('user.username');
    }
}

export default UserService;
