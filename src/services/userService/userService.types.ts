import User from '../../database/entity/user';
import { ICreateUserInput, ICredentials } from '../../types';

export interface IUserService {
    getUser(id: number): Promise<User>;
    getUserWithLogin(credentials: ICredentials): Promise<User>;
    getUsers(): Promise<User[]>;
    createUser(userInput: ICreateUserInput): Promise<void>;
    registerUser(userInput: ICreateUserInput): Promise<void>;
}
