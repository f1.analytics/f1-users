import { errorCodes, statusCodes } from '../errors.types';
import { RequestError } from './requestError';

export class EmailTakenError extends RequestError {
    public constructor(...args: any) {
        super(...args);

        this.statusCode = statusCodes.CONFLICT;
        this.errorCode = errorCodes.EMAIL_TAKEN;
    }
}
