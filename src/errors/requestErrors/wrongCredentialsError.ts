import { errorCodes, statusCodes } from '../errors.types';
import { RequestError } from './requestError';

export class WrongCredentialsError extends RequestError {
    public constructor(...args: any) {
        super(...args);

        this.statusCode = statusCodes.UNAUTHORIZED;
        this.errorCode = errorCodes.WRONG_CREDENTIALS;
    }
}
