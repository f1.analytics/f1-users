export interface IError {
    getMessage(): string;
}

export enum statusCodes {
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    NOT_FOUND = 404,
    CONFLICT = 409,
    INTERNAL_SERVER_ERROR = 500,
}

export enum errorCodes {
    BAD_REQUEST = 'BadRequest',
    EMAIL_TAKEN = 'EmailTaken',
    INTERNAL_SERVER_ERROR = 'InternalServerError',
    NOT_FOUND = 'NotFound',
    UNAUTHORIZED = 'Unauthorized',
    USER_NOT_FOUND = 'UserNotFound',
    USERNAME_TAKEN = 'UsernameTaken',
    WRONG_CREDENTIALS = 'WrongCredentials',
}
