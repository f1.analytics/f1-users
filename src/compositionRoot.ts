import DashboardsClient, { IDashboardsClient } from './clients/dashboardsClient';
import Persistence, { IPersistence } from './database/persistence';
import RestService, { IRestService } from './restService';
import TokenController from './restService/controllers/tokenController';
import UserController from './restService/controllers/userController';
import RequestHandlersManager, { IRequestHandlerManager } from './restService/requestHandlersManager';
import ErrorHandler from './restService/requestHandlersManager/handlers/errorHandler';
import { IErrorHandler } from './restService/requestHandlersManager/handlers/errorHandler';
import { ErrorHandlerHelperFactory, IErrorHandlerHelperFactory } from './restService/requestHandlersManager/handlers/errorHandler/errorHandlerHelper';
import AuthorizationHandler, { IAuthorizationHandler } from './restService/requestHandlersManager/handlers/requestHandlers/authorizationHandler';
import BodyParseHandler, { IBodyParseHandler } from './restService/requestHandlersManager/handlers/requestHandlers/bodyParseHandler';
import NotFoundHandler from './restService/requestHandlersManager/handlers/requestHandlers/notFoundHandler';
import { INotFoundHandler } from './restService/requestHandlersManager/handlers/requestHandlers/notFoundHandler';
import Server from './server';
import TokenService from './services/tokenService';
import { ITokenService } from './services/tokenService/tokenService.types';
import UserService, { IUserService } from './services/userService';
import Logger, { ILogger } from './tools/logger';
import PasswordManager, { IPasswordManager } from './tools/passwordManager';
import RequestValidator from './tools/requestValidator/requestValidator';
import { IRequestValidator } from './tools/requestValidator/requestValidator.types';
import SchemaGenerator, { ISchemaGenerator } from './tools/schemaGenerator';
import SchemaValidator, { ISchemaValidator } from './tools/schemaValidator';
import SecretsManger, { ISecretsManager } from './tools/secretsManager';
import TokenManager, { ITokenManager } from './tools/tokenManager';
import { IConfig } from './types';

class CompositionRoot {
    private logger: ILogger;
    private secretsManager: ISecretsManager;
    private tokenManager: ITokenManager;
    private passwordManager: IPasswordManager;
    private schemaGenerator: ISchemaGenerator;
    private schemaValidator: ISchemaValidator;
    private requestValidator: IRequestValidator;
    private persistence: IPersistence;
    private dashboardsClient: IDashboardsClient;
    private errorHandlerHelperFactory: IErrorHandlerHelperFactory;
    private bodyParseHandler: IBodyParseHandler;
    private notFoundHandler: INotFoundHandler;
    private authorizationHandler: IAuthorizationHandler;
    private errorHandler: IErrorHandler;
    private requestHandlerManager: IRequestHandlerManager;
    private userService: IUserService;
    private tokenService: ITokenService;
    private restService: IRestService;
    private server: Server;

    public async createServer(config: IConfig): Promise<void> {
        this.logger = new Logger(config.logger);
        this.secretsManager = new SecretsManger(this.logger);
        this.tokenManager = new TokenManager(this.secretsManager, config.jwt);
        this.passwordManager = new PasswordManager();
        this.schemaGenerator = new SchemaGenerator();
        this.schemaValidator = new SchemaValidator();
        this.requestValidator = new RequestValidator(this.schemaGenerator, this.schemaValidator);

        this.persistence = new Persistence(config.persistence, this.secretsManager);
        await this.persistence.init();

        this.dashboardsClient = new DashboardsClient(config.api.dashboards, this.tokenManager);

        this.errorHandlerHelperFactory = new ErrorHandlerHelperFactory(this.logger);

        this.bodyParseHandler = new BodyParseHandler();
        this.notFoundHandler = new NotFoundHandler();
        this.authorizationHandler = new AuthorizationHandler(this.tokenManager);
        this.errorHandler = new ErrorHandler(this.errorHandlerHelperFactory);
        this.requestHandlerManager = new RequestHandlersManager(
            this.bodyParseHandler,
            this.notFoundHandler,
            this.authorizationHandler,
            this.errorHandler,
        );

        this.userService = new UserService(this.passwordManager, this.dashboardsClient);
        this.tokenService = new TokenService(this.tokenManager, this.userService);

        const userController = new UserController(this.requestHandlerManager, this.requestValidator, this.userService);
        const tokenController = new TokenController(this.requestValidator, this.tokenService);

        this.restService = new RestService(
            config.server,
            this.logger,
            this.requestHandlerManager,
            [userController, tokenController],
        );

        this.server = new Server(this.restService);
    }

    public getServer(): Server {
        return this.server;
    }

    public getRestService(): IRestService {
        return this.restService;
    }

    public getUserService(): IUserService {
        return this.userService;
    }

    public getPasswordManager(): IPasswordManager {
        return this.passwordManager;
    }

    public getTokenManager(): ITokenManager {
        return this.tokenManager;
    }
}

export default CompositionRoot;
