export interface IPasswordManager {
    encodePassword(password: string): string;
    validatePassword(password: string, hash: string): Promise<void>;
}
