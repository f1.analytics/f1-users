import bcrypt from 'bcrypt';

import { WrongCredentialsError } from '../../errors/requestErrors';
import { IPasswordManager } from './passwordManager.types';

class PasswordManager implements IPasswordManager {
    private readonly saltRounds: number = 10;

    public encodePassword(password: string): string {
        return bcrypt.hashSync(password, this.saltRounds);
    }

    public async validatePassword(password: string, hash: string): Promise<void> {
        const result = await bcrypt.compare(password, hash);
        if (!result) {
            throw new WrongCredentialsError('Wrong credentials');
        }
    }
}

export default PasswordManager;
