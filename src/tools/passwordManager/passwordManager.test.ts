import bcrypt from 'bcrypt';

import { WrongCredentialsError } from '../../errors/requestErrors';
import PasswordManager from './passwordManager';

describe('Password manager tests', () => {
    const encodedPassword = '133as';
    const password = 'password123';
    const passwordManager = new PasswordManager();

    describe('encodePassword function', () => {
        it('Should return encoded password', () => {
            jest.spyOn(bcrypt, 'hashSync').mockImplementation(() => encodedPassword);

            const returnedPassword = passwordManager.encodePassword(password);

            expect(bcrypt.hashSync).toHaveBeenCalledTimes(1);
            expect(bcrypt.hashSync).toHaveBeenCalledWith(password, 10);
            expect(returnedPassword).toBe(encodedPassword);
        });
    });
    describe('validatePasswordAgainstHash function', () => {
        it('Should validate password against encoded password', async () => {
            jest.spyOn(bcrypt, 'compare').mockImplementation(() => new Promise((resolve) => {
                resolve(true);
            }));

            await passwordManager.validatePassword(password, encodedPassword);

            expect(bcrypt.compare).toHaveBeenCalledTimes(1);
            expect(bcrypt.compare).toHaveBeenCalledWith(password, encodedPassword);
        });
        it('Should throw error if passwords do not compare', async () => {
            jest.spyOn(bcrypt, 'compare').mockImplementation(() => new Promise((resolve) => {
                resolve(false);
            }));

            return expect(passwordManager.validatePassword(password, encodedPassword))
                .rejects.toThrow(WrongCredentialsError);
        });
    });
});
