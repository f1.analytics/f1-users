import { resolve } from 'path';
import { Definition, generateSchema, getProgramFromFiles, Program } from 'typescript-json-schema';

class SchemaGenerator {
    private readonly program: Program;

    public constructor() {
        this.program = this.initProgram();
    }

    public generate(schemaName: string): Definition {
        return generateSchema(
            this.program,
            schemaName,
            { required: true },
        );
    }

    private initProgram(): Program {
        return getProgramFromFiles(
            [resolve('./src/types/schema.ts')],
            {
                strictNullChecks: true,
            },
        );
    }
}

export default SchemaGenerator;
