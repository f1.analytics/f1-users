import { ISchemaGenerator } from '../schemaGenerator';
import { ISchemaValidator } from '../schemaValidator';
import { IRequestValidator } from './requestValidator.types';

class RequestValidator implements IRequestValidator {
    private readonly schemaGenerator: ISchemaGenerator;
    private readonly schemaValidator: ISchemaValidator;

    public constructor(schemaGenerator: ISchemaGenerator, schemaValidator: ISchemaValidator) {
        this.schemaGenerator = schemaGenerator;
        this.schemaValidator = schemaValidator;
    }

    public validateRequestData(data: object, typeName: string): void {
        const schema = this.schemaGenerator.generate(typeName);
        this.schemaValidator.validate(schema, data);
    }
}

export default RequestValidator;
