import jwt from 'jsonwebtoken';

import { UnauthorizedError } from '../../errors';
import { IJwtConfig } from '../../types';
import { ISecretsManager } from '../secretsManager';
import { ITokenManager } from './tokenManager.types';

class TokenManager implements ITokenManager {
    private readonly secretsManager: ISecretsManager;
    private readonly config: IJwtConfig;
    private readonly jwtPublicKey: string;
    private readonly jwtPrivateKey: string;
    private readonly staticTokens: string[];

    public constructor(secretsManager: ISecretsManager, config: IJwtConfig) {
        this.secretsManager = secretsManager;
        this.config = config;
        this.jwtPublicKey = this.readJwtPublicKey();
        this.jwtPrivateKey = this.readJwtPrivateKey();
        this.staticTokens = this.readStaticTokens();
    }

    public createJwtAccessToken(payload: object): string {
        return jwt.sign(
            {
                payload,
            },
            this.jwtPrivateKey,
            {
                algorithm: 'RS256',
                expiresIn: this.config.accessTokenLifeTime,
            },
        );
    }

    public createJwtRefreshToken(payload: object): string {
        return jwt.sign(
            {
                isRefreshToken: true,
                payload,
            },
            this.jwtPrivateKey,
            {
                algorithm: 'RS256',
                expiresIn: this.config.refreshTokenLifeTime,
            },
        );
    }

    public validateJwtToken(token: string): any {
        try {
            return jwt.verify(token, this.jwtPublicKey);
        } catch (error) {
            throw new UnauthorizedError(`Invalid json web token: ${error.message}`);
        }
    }

    public validateStaticToken(token: string): void {
        if (!this.staticTokens.includes(token)) {
            throw new UnauthorizedError(`Invalid static token: ${token}`);
        }
    }

    public getStaticToken(): string {
        // Random item
        return this.staticTokens[Math.floor(Math.random() * this.staticTokens.length)];
    }

    private readJwtPublicKey(): string {
        return this.secretsManager.get('JWT_PUBLIC_KEY');
    }

    private readJwtPrivateKey(): string {
        return this.secretsManager.get('JWT_PRIVATE_KEY');
    }

    private readStaticTokens(): string[] {
        return this.secretsManager.get('STATIC_TOKENS').split(',');
    }
}

export default TokenManager;
