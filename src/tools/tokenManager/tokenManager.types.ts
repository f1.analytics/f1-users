export interface ITokenManager {
    createJwtAccessToken(payload: object): string;
    createJwtRefreshToken(payload: object): string;
    validateJwtToken(token: string): any;
    validateStaticToken(token: string): void;
    getStaticToken(): string;
}
