import jwt from 'jsonwebtoken';

import { UnauthorizedError } from '../../errors/requestErrors';
import TokenManager from './tokenManager';

describe('Token manager tests', () => {
    const mockSecretKey = 'secretKey';
    const mockToken = 'token';
    const secretsManager = {
        get: () => mockSecretKey,
    };
    const config = {
        accessTokenLifeTime: '1d',
        refreshTokenLifeTime: '7d',
    };
    const tokenManager = new TokenManager(secretsManager, config);

    describe('createJwtAccessToken method', () => {
        it('Should create jwt access token', () => {
            const payload = { email: 'test' };
            const spy = jest.spyOn(jwt, 'sign').mockImplementation(() => mockToken);
            expect(tokenManager.createJwtAccessToken(payload)).toEqual(mockToken);
            expect(spy).toHaveBeenCalledWith(
                { payload },
                mockSecretKey,
                {
                    algorithm: 'RS256',
                    expiresIn: config.accessTokenLifeTime,
            });
        });
    });
    describe('createJwtRefreshToken method', () => {
        it('Should create jwt access token', () => {
            const payload = { email: 'test' };
            const spy = jest.spyOn(jwt, 'sign').mockImplementation(() => mockToken);
            expect(tokenManager.createJwtRefreshToken(payload)).toEqual(mockToken);
            expect(spy).toHaveBeenCalledWith(
                {
                    isRefreshToken: true,
                    payload,
                },
                mockSecretKey,
                {
                    algorithm: 'RS256',
                    expiresIn: config.refreshTokenLifeTime,
                });
        });
    });
    describe('validateJwtToken method', () => {
        it('Should verify jwt', () => {
            const mockPayload = { test: 123 };
            const spy = jest.spyOn(jwt, 'verify').mockImplementation(() => mockPayload);
            expect(tokenManager.validateJwtToken(mockToken)).toEqual(mockPayload);
            expect(spy).toHaveBeenCalledWith(mockToken, mockSecretKey);
        });
        it('Should throw error if token not valid', () => {
            const mockError = new Error('invalidToken');
            jest.spyOn(jwt, 'verify').mockImplementation(() => {
                throw mockError;
            });
            expect(() => tokenManager.validateJwtToken(mockToken)).toThrow(UnauthorizedError);
        });
    });
    describe('getStaticToken method', () => {
        const mockStaticTokens = 'token1,token2';
        const secretsManager2 = {
            get: () => mockStaticTokens,
        };
        const tokenManager2 = new TokenManager(secretsManager2, config);
        it('Should validate static token', () => {
            expect(() => tokenManager2.validateStaticToken('token1')).not.toThrow();
        });
        it('Should throw error on invalid token', () => {
            expect(() => tokenManager2.validateStaticToken('invalidToken')).toThrow(UnauthorizedError);
        });
    });
});
