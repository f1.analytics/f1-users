import winston from 'winston';

import { ILoggerConfig } from '../../types';
import { ILogger, ILoggerMeta } from './logger.types';

class Logger implements ILogger {
    private readonly logger: winston.Logger;

    public constructor(config: ILoggerConfig) {
        this.logger = this.createLogger(config);
    }

    public info(message: string): void {
        this.logger.info(message);
    }

    public warning(message: string, meta?: ILoggerMeta): void {
        this.logger.warn(this.constructWinstonMessage(message, meta), this.constructWinstonMeta(meta));
    }

    public error(message: string, meta?: ILoggerMeta): void {
        this.logger.error(this.constructWinstonMessage(message, meta), this.constructWinstonMeta(meta));
    }

    public getWinstonLogger(): winston.Logger {
        return this.logger;
    }

    private createLogger(config: ILoggerConfig): winston.Logger {
        return winston.createLogger({
            format: winston.format.combine(
                winston.format(this.errorStackTracerFormat)(),
                winston.format.simple(),
            ),
            level: config.level,
            transports: [
                new winston.transports.Console(),
            ],
        });
    }

    private constructWinstonMessage(message: string, meta?: ILoggerMeta): string {
        if (meta && meta.req) {
            const { body, originalUrl } = meta.req;
            return `${message}:\n${meta.req.method} ${originalUrl}, ${JSON.stringify(body)}`;
        }
        return message;
    }

    private constructWinstonMeta(meta?: ILoggerMeta): object {
        return meta ? {
            error: meta.error ? meta.error : undefined,
        } : undefined;
    }

    private errorStackTracerFormat(info: winston.LogEntry): winston.LogEntry {
        if (info.error && info.error instanceof Error) {
            if (info.level === 'error') {
                info.message = `${info.message}:\n${info.error.stack}`;
            } else if (info.level === 'warn') {
                info.message = `${info.message}: ${info.error.message}`;
            }
            delete info.error;
        }
        return info;
    }
}

export default Logger;
