import { Router } from 'express';

export interface IController {
    resolveRouter(): Router;
    getRoute(): string;
}
