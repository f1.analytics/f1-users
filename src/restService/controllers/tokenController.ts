import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { ITokenService } from '../../services/tokenService/tokenService.types';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import Controller from './controller';

class TokenController extends Controller {
    private readonly tokenService: ITokenService;

    public constructor(
        requestValidator: IRequestValidator,
        tokenService: ITokenService,
    ) {
        super(requestValidator);
        this.tokenService = tokenService;
        this.route = '/tokens';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.post('/', this.postToken());
        router.post('/:id', this.postRefreshToken());

        return router;
    }

    private postToken(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const credentials = req.body;
                this.requestValidator.validateRequestData(credentials, 'ICreateTokensInput');
                const tokensData = await this.tokenService.createTokens(credentials);
                res.send(tokensData);
            } catch (error) {
                next(error);
            }
        };
    }

    private postRefreshToken(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                this.requestValidator.validateRequestData(req.body, 'IRefreshTokenInput');
                const { refreshToken } = req.body;
                const accessToken = await this.tokenService.refreshToken(refreshToken);
                res.send({ accessToken });
            } catch (error) {
                next(error);
            }
        };
    }
}

export default TokenController;
