import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { UnauthorizedError } from '../../errors/requestErrors';
import { IUserService } from '../../services/userService';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import Controller from './controller';

class UserController extends Controller {
    private readonly requestHandlerManager: IRequestHandlerManager;
    private readonly userService: IUserService;

    public constructor(
        requestHandlerManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
        userService: IUserService,
    ) {
        super(requestValidator);
        this.requestHandlerManager = requestHandlerManager;
        this.userService = userService;
        this.route = '/users';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.post('/', this.postUser());
        router.use(this.requestHandlerManager.getAuthorizationHandler());
        router.get('/:id', this.getUser());

        return router;
    }

    private postUser(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const userInput = req.body;
                this.requestValidator.validateRequestData(userInput, 'ICreateUserInput');

                await this.userService.registerUser(userInput);

                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private getUser(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const userId = Number(req.params.id);
                if (userId !== req.user.id && !req.user.isSuperuser) {
                    throw new UnauthorizedError(`User ${req.user.id} is not allowed to access user ${userId}`);
                }
                const user = await this.userService.getUser(userId);
                res.send(user);
            } catch (error) {
                next(error);
            }
        };
    }
}

export default UserController;
