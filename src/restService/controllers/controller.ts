import { Router } from 'express';

import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IController } from './controller.types';

abstract class Controller implements IController {
    protected readonly requestValidator: IRequestValidator;
    protected route: string;

    public constructor(requestValidator: IRequestValidator) {
        this.requestValidator = requestValidator;
    }

    public abstract resolveRouter(): Router;

    public getRoute(): string {
        return this.route;
    }
}

export default Controller;
