import { NextFunction, Request, Response } from 'express';

export interface IErrorHandlerHelper {
    handleError(error: Error, req: Request, res: Response, next: NextFunction): void;
}

export interface IErrorHandlerHelperFactory {
    create(error: Error): IErrorHandlerHelper;
}
