import { NextFunction, Request, Response } from 'express';

import { ILogger } from '../../../../../tools/logger';
import { IErrorHandlerHelper } from './errorHandlerHelper.types';

export default abstract class ErrorHandlerHelper implements IErrorHandlerHelper {
    protected readonly logger: ILogger;

    public constructor(logger: ILogger) {
        this.logger = logger;
    }

    public abstract handleError(error: Error, req: Request, res: Response, next: NextFunction): void;
}
