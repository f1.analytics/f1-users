import NotFoundHandler from './notFoundHandler';

export * from './notFoundHandler.types';
export default NotFoundHandler;
