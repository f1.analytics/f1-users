import { NextFunction, Request, RequestHandler, Response } from 'express';

import { NotFoundError } from '../../../../../errors/requestErrors';
import { INotFoundHandler } from './notFoundHandler.types';

class NotFoundHandler implements INotFoundHandler {
    public getHandler(): RequestHandler {
        return (req: Request, res: Response, next: NextFunction) => {
            const message = `${req.originalUrl} path not found`;
            const error = new NotFoundError(message);
            next(error);
        };
    }
}

export default NotFoundHandler;
