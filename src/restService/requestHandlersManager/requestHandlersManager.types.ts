import { ErrorRequestHandler, RequestHandler } from 'express';

export interface IRequestHandlerManager {
    getBodyParseHandler(): RequestHandler;
    getNotFoundHandler(): RequestHandler;
    getAuthorizationHandler(): RequestHandler;
    getErrorHandler(): ErrorRequestHandler;
}
