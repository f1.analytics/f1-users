import { Express } from 'express';

export interface IRestService {
    init(): void;
    start(): void;
    getApp(): Express;
}
